function MyPromise(executor) {
  // this = {}
  // this.__proto__ = MyPromise.prototype
  // return this

  if (typeof executor !== 'function') {
    throw new Error('Executor must be a function')
  }

  this.resolvedValue = null
  this.hasPromiseBeenResolved = false
  this.callback = null

  this.then = (callback) => {
    this.callback = callback
    if (this.hasPromiseBeenResolved) {
      callback(this.resolvedValue)
    }
  }

  this.resolve = (resolvedValue) => {
    this.hasPromiseBeenResolved = true
    this.resolvedValue = resolvedValue
    if (typeof this.callback === 'function') {
      this.callback(resolvedValue)
    }
  }

  executor(this.resolve)
}

//MyPromise.prototype.blah = "I exist on the MyPromise Prototype";

module.exports = MyPromise
