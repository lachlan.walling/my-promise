const MyPromise = require('./my-promise')

// test for a promise that does not resolve immediately

describe('my-promise', () => {
  it('MyPromise should be a constructor', () => {
    expect(typeof MyPromise).toBe('function')
  })

  it('MyPromise should throw an error when the executor is not a function', () => {
    expect(() => new MyPromise()).toThrow('Executor must be a function')
  })

  it('MyPromise should have a then method', () => {
    const executor = () => {}
    const myPromise = new MyPromise(executor)
    expect(Object.getPrototypeOf(myPromise)).toBe(MyPromise.prototype)
    expect(typeof myPromise.then).toBe('function')
  })

  describe('Given an executor that resolves immediately', () => {
    it('MyPromise should invoke the registered callback passing the resolved value', (done) => {
      const promise = new MyPromise((resolve) => {
        resolve('Resolved value')
      })

      //callback passed to .then should be called
      const callback = (value) => {
        expect(value).toBe('Resolved value')
        done()
      }

      const wrappedCallback = jest.fn(callback)
      promise.then(wrappedCallback)

      expect(wrappedCallback).toHaveBeenCalled()
      //await expect(promise).resolves.toBe('Resolved value')
    })
  })

  describe('Given an executor that defers resolution', () => {
    describe('and the callback is registered before the promise is resolved', () => {
      it('MyPromise should invoke the registered callback passing the resolved value', (done) => {
        const executor = (resolve) => {
          // Don't want to lag the test
          setTimeout(() => resolve('done'), 100)
        }
        const promise = new MyPromise(executor)
        const resolutionHandler = (resolvedValue) => {
          expect(resolvedValue).toBe('done')
          done()
        }
        promise.then(resolutionHandler)
      })
    })
    describe('and the callback is registered after the promise is resolved', () => {
      it('MyPromise should invoke the registered callback passing the resolved value', (done) => {
        const executor = (resolve) => setTimeout(() => resolve('done'), 100)
        const promise = new MyPromise(executor)
        const resolutionHandler = (resolvedValue) => {
          expect(resolvedValue).toBe('done')
          done()
        }
        setTimeout(() => promise.then(resolutionHandler), 101)
      })
    })
  })
})
